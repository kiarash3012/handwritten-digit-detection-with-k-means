import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist
from sklearn.cluster import MiniBatchKMeans
from sklearn import metrics


(x_train, y_train), (x_test, y_test) = mnist.load_data()

print('Training Data: {}'.format(x_train.shape))
print('Training Labels: {}'.format(y_train.shape))
print('Testing Data: {}'.format(x_test.shape))
print('Testing Labels: {}'.format(y_test.shape))

x = x_train.reshape(len(x_train), -1)
x = x.astype(float) / 255.
y = y_train

x_test = x_test.reshape(len(x_test), -1)
x_test = x_test.astype(float) / 255.
n_digits = len(np.unique(y_test))


def infer_cluster_labels(model, actual_labels):
    """
    fix name labels to real ones
    :param model: kmeans model
    :param actual_labels:
    :return:
    """
    inferred_labels = {}
    for i in range(model.n_clusters):
        labels = []
        index = np.where(model.labels_ == i)
        labels.append(actual_labels[index])
        # find most common label
        if len(labels[0]) == 1:
            count = np.bincount(labels[0])
        else:
            count = np.bincount(np.squeeze(labels))

        # assign cluster to cal in inferred labels
        if np.argmax(count) in inferred_labels:
            inferred_labels[np.argmax(count)].append(i)
        else:
            inferred_labels[np.argmax(count)] = [i]

    return inferred_labels


def infer_data_labels(x_labels, c_labels):
    """
    determine label for each array based on the cluster that assigned to
    :param x_labels:
    :param c_labels:
    :return:predicted labels for each array
    """
    predicted_labels = np.zeros(len(x_labels)).astype(np.uint8)

    for i, cluster in enumerate(x_labels):
        for key, val in c_labels.items():
            if cluster in val:
                predicted_labels[i] = key

    return predicted_labels


def calculate_metrics(model, labels):
    print('Number of clusters: {}'.format(model.n_clusters))
    print('Inertia: {}'.format(model.inertia_))
    print('Homogeneity: {}'.format(metrics.homogeneity_score(labels, model.labels_)))


clusters = [i for i in range(10, 310, 10)]
for n_clusters in clusters:
    estimator = MiniBatchKMeans(n_clusters=n_clusters)
    estimator.fit(x)
    print('#'*50)
    print('For train data:')
    calculate_metrics(estimator, y)
    cluster_labels = infer_cluster_labels(estimator, y)
    predicted_y = infer_data_labels(estimator.labels_, cluster_labels)
    print('Accuracy: {}\n'.format(metrics.accuracy_score(y, predicted_y)))
    print('For test data:')
    test_clusters = estimator.predict(x_test)
    predicted_labels = infer_data_labels(test_clusters, cluster_labels)
    print('Testing Accuracy: {}'.format(metrics.accuracy_score(y_test, predicted_labels)))

# best trade of computation time - accuracy for number of clusters in my case: 230

# visualize cluster centers (only for demo purpose
kmeans = MiniBatchKMeans(n_clusters=40)
kmeans.fit(x)
centroids = kmeans.cluster_centers_
cluster_labels = infer_cluster_labels(kmeans, y)

images = centroids.reshape(40, 28, 28)
images *= 255
images = images.astype(np.uint8)
fig, axs = plt.subplots(6, 6, figsize=(20, 20))
plt.gray()

for i, ax in enumerate(axs.flat):
    for key, val in cluster_labels.items():
        if i in val:
            ax.set_title('Inferred label: {}'.format(key))

    ax.matshow(images[i])
    ax.axis('off')

plt.show()
