# In this project in tried to work on mnist dataset (only KMeans solution)
## Some NN solutions have reached accuracy of 99.99% on this dataset but my best score is 93%.
---
### Sample of what KMeans think of numbers(for some numbers we have multiple images => number of clusters = 40):
![fig sample](images/fig.png)
#### Isn't this super cool!?
#### Clear images mean that ai have clear (more accurate vision) of what a number means, and more blurry image means vision(understanding) of a number is not so clear therefore more mistakes can happen for this detection.
 